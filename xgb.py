import pandas as pd
from xgboost import XGBClassifier
from sklearn.model_selection import train_test_split, KFold
from sklearn.metrics import confusion_matrix, mean_squared_error
from sklearn.model_selection import RandomizedSearchCV
from sklearn.preprocessing import LabelEncoder

df_train = pd.read_csv('data/train-2.csv')
df_test = pd.read_csv('data/test-2.csv')

X= df_train.drop(['Survived'],1)
y= df_train['Survived']

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33, random_state=42)
model = XGBClassifier(n_estimators=18, max_depth=7, learning_rate=0.4, colsample_bytree=0.6)

# params = {
#     'n_estimators': range(8, 20),
#     'max_depth': range(6, 10),
#     'learning_rate': [.4, .45, .5, .55, .6],
#     'colsample_bytree': [.6, .7, .8, .9, 1]
# }

# xgb_random = RandomizedSearchCV(param_distributions=params, 
#                                     estimator = model, scoring = "accuracy", 
#                                     verbose = 1, n_iter = 50, cv = 4)
# xgb_random.fit(X_train, y_train)
# print("Best parameters found: ", xgb_random.best_params_)
# print("Best accuracy found: ", xgb_random.best_score_)

model.fit(X_train, y_train)
print("training set accuracy:",model.score(X_train, y_train))
print("test set accuracy:",model.score(X_test,y_test))


le = LabelEncoder()
df_test['Sex'] = le.fit_transform(df_test['Sex'])
result = model.predict(df_test)
submission = pd.read_csv('data/submission.csv')
submission['Survived']=result
submission.to_csv('data/submissions/submission_xgb.csv',index=False)