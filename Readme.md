# Titanic


## Analyse exploratoire et Nettoyage

Dans le set d'entrainement: 
- Vérification du nombre de valeur nulles par colonne
    - Age : 177
    - Cabin : 687
    - Embarked : 2

- La colonne Cabin représentant le numéro de cabine ayant plus de 70% de valeurs nulles, nous avons choisi de la supprimer

- Concernant l'âge, après exploration des données nous avons choisi de remplacer les valeurs nulles par la médiane de l'âge en fonction du sexe et de la classe de voyage

- Suppression des colonnes ne présentant pas d'interêts pour la prédiction après les avoir étudiées
    
    - PassengerId &#8594; Doublon de l'index
    - Ticket &#8594; Numéro de tick
    - Name &#8594; Identité du passager

- Par intuition, nous avons voulu verifier si le taux de survie chez les passagers agès de moins de 18 ans était plus important et effectivement il est de 53,9% chez cet echantillon contre 36% chez les passagers agès de plus de 18 ans cependant il n'y a que 113 passagers agès de moins de 18ans.

- Nous avons également fait une matrice de corrélation à partir de laquelle nous avons tiré les idées suivantes:
    - Le fait de survivre est plutot bien corrélé (0.54) au fait d'être une femme
    - L'âge est très peu correlé au taux de survie (0.064) car l'échantillon susmentionné ne représente qu'un faible pourcentage de l'effectif total.
    - La colonne Fare à une correlation avec la survie (0.26). Fare à une forte corrélation negative avec Class, après avoir regardé plus en détail dans le dataset, nous avons par exemple des passagers de premières classe ayant un 0 dans la colonne Fare. Après recherche, il s'avère que certains passagers n'ont pas payès leur billet (Comme par exemple le president de la societe propriétaire du Titanic) et d'autres ont pu obtenir des réductions. Cette colonne à donc très peu de valeur pour estimer les chances de survie.
    - Les colonnes SibSp et Parch sont très faiblement correlées à la survie. Ces deux colonnes indiquant s'il y a des membres de la famille à bord ensemble, nous nous étions intuitivement dit qu'elles n'auraient aucune incidence sur la survie ce qui nous est confirmé ici.
    - La pclass a une assez forte corrélation négative (-0.34) avec la survie, les valeurs dans class étant ```1, 2 ou 3``` pour première , deuxième et 3ème classe. Après avoir refait une matrice en divisant la colonne en 3 colonnes nous en avons tiré la conclusion suivante: Les passagers en 3e classe ont moins de chance de survivre que les passagers en 1ere classe.
    En effet, lorsque l'on regarde le plan du bateau, le point d'impact avec l'iceberg s'est fait entre les couchettes de 3eme classe et le reste du bateau. Celles-ci étaient situées sous le pont, du coté par lequel le bateau a commencé à couler. De plus, les cabines de première classe avaient un accès plus simple au pont sur lequel se situaientt les canots de sauvetage ainsi qu'à l'équipage [source](https://whyfiles.org/2010/studying-survival-on-a-sinking-ship/index.html)
    - La colonne Embarked représente le port depuis lequel ont embarqué les passagers. Cette colonne a donc une corrélation faible (0.16) avec la classe et le sexe (0.11) ainsi qu'une faible corrélation négative avec la survie (-0.16). Nous avons refait une matrice en divisant la colonne Embarked en 3 colonnes différentes pour les 3 ports d'embarcation et nous pu en tirer l'information suivante: Le point d'embarcation est bien corrélé à la classe, l'embarcation depuis Cherbourg a une corrélation de 0.33 avec le fait d'être en 1ere classe tandis que Southampton a une corrélation de 0.2 avec la 2de classe et Queenstown 0.25 avec la 3e classe. 
    Nous avons donc fait le choix d'éliminer cette colonne.

- Les données confirment bien la règle appliquée pour les canots de sauvetage "Les femmes et les enfants d'abord".

Après cette étape, nous avons donc gardé les colonnes suivantes:

|   Id	|   Survived	| Sex 	|   Age	|   Pclass_1	|   Pclass_2	|   Pclass_3	|
|:-:	|:-:	|:-:	|:-:	|:-:	|:-:	|:-:	|

<br/><br/>

Après avoir fait quelques soumissions(avec différents algorithmes et différentes combinaisons d'hyperparametres) avec cette combinaison de colonnes, notre meilleur résultat a été de 0.76555.
<br/><br/>

## Seconde Itération

### Selection de l'algorithme

Pour nous aider dans cette décision, nous avons utilisé le module ``` sklearn.model_selection.KFold ```. Tout au long de nos modifications sur les datasets, nous avons répété cette opération avant de faire des soumissions.

Le choix était entre les modules suivants:
- ```sklearn.ensemble.GradientBoostingClassifier```
- ```sklearn.ensemble.AdaBoostClassifier``` &rarr; avec un decision tree en base estimator
- ```sklearn.ensemble.RandomForestClassifier```
- ```xgboost.XGBClassifier```
- ``` sklearn.discriminant_analysis.LinearDiscriminantAnalysis ```
- ``` sklearn.tree.DecisionTreeClassifier ```

Lors de cette étape, nous avons réalisé que certains modèles overfittaient très facilement (avec les hyperparamètres par défaut en tout cas), ils obtenaient un score de précision de 1 sur le training set mais un score <0.8 sur le jeu de test et un score <0.75 en cas de soumission. C'était le cas pour:
- Xgboost (training accuracy:  0.9949664429530202)
- RandomForest (training accuracy:  1.0)
- DecisionTree (training accuracy:  1.0)
- AdaBoost (training accuracy:  1.0)

Nous n'avons pas trouvé d'explications autre que la taille du jeu de données.


### Reconfiguration des datasets
 Suite à ce résultat, nous avons essayé de nombreuses pistes d'améliorations mais n'avons rien obtenu de concret. Par exemple nous avons créé une nouvelle colonne 'Alone' qui contenait un booléen indiquant si le passager etait seul ou accompagné en fonction des colonnes Sibsp et Parch. 



Cette fois-ci, nous avons tout d'abord remis la colonne Fare en remplacant les valeurs nulles par les valeures médianes en fonction de Pclass et Sex. 


Après avoir refait quelques soumissions avec les algorithmes mais nos résultats ne se sont pas améliorès. Nous avons donc décidé de regarder plus en détail au niveau des modèles quels étaient les poids donnés à chaque feature et nous avons été assez surpris de voir que globalement nos modèles les classaient globalement ainsi: Fare > Age > Pclass > Sex. Sauf XGBoost qui donne Fare > Age > Embarked > Sex.

Or d'après notre matrice de corrélation la variable Sex est la plus fortement corrélée à la survie suivie par Fare, Pclass puis Embarked et enfin Age.

Nous avons donc essayé de faire en sorte qu'au moins Sex soit la feature avec le plus de poids. Pour cela, nous avons d'abord séparé la colonne Sex en deux colonnes: Sex_female et Sex_male. Nous avons pu voir des changements dans les poids puisque Sex_Female gagnait de l'importance par rapport à Sex_male ce qui améliorait legerement nos resultats. <br></br>
Cette amélioration n'étant pas suffisante, nous avons tout d'abord supprimé la colonne Fare ce qui n'a pas semblé améliorer nos résultats.   
Nous avons ensuite cherché comment modifier les hyperparametres pour que les poids correspondent d'avantage à l'idée que nous nous sommes faite. Pour cela, nous avons choisi de rester sur le modèle qui en était le plus proche: GradienBoostClassifier.  
En divisant par deux la valeur par défaut (100) de n_estimators, nous sommes parvenus à des poids qui nous paraissaient plus cohérents et avons refait une soumission pour laquelle nous avons obtenu le score suivant: 0.78708

Nous avons ensuite remis la colonne fare tout en continuant de modifier les valeurs des hyperparamètres mais nous n'avons pas réussi à nous rapprocher du score obtenu precedemment dans cette configuration. 

Nous avons aussi souhaité refaire quelques test avec des données scalées mais nous n'avons pas obtenu de résultats probants avec ```LDA``` qui était le plus sensible à ces changements dans notre liste de possibilités.

Nous avons donc terminé avec les colonnes suivantes:

|   Id	|   Survived	|   Sex_female    | Sex_male     |   Age	|   Pclass_1	|   Pclass_2	|   Pclass_3	|
|:-:	|:-:	|:-:	|:-:	|:-:	|:-:	|:-:	|  :-:	| 

<br></br>
Les scores moyen des modèles à l'issue du dernier kfold étaient les suivants:
|   	|   	|
|---	|---	|
|   Xgboost   |   0.7651 (+/- 0.0552) |
|   GradientBoostingClassifier  |   0.7953 (+/- 0.0458) |
|   LDA   |   0.7802 (+/- 0.0536) |
|   RandomForestClassifier   |   0.7920 (+/- 0.0422) |
|   AdaBoostClassifier   |   0.7499 (+/- 0.0351) |
|   DecisionTreeClassifier   |   0.7516 (+/- 0.0469) |
