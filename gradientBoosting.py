from tkinter import Grid
import pandas as pd
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.model_selection import train_test_split
from sklearn.model_selection import RandomizedSearchCV, GridSearchCV
from sklearn.preprocessing import LabelEncoder

df_train = pd.read_csv('data/train-2.csv', index_col=0)
df_test = pd.read_csv('data/test-2.csv', index_col=0)

X= df_train.drop(['Survived'],1)
y= df_train['Survived']

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33, random_state=42)

model = GradientBoostingClassifier(n_estimators=50, learning_rate=0.1, max_depth=3) # n_estimators=50, learning_rate=0.1

# params = {
#     'n_estimators': [25, 50, 100, 150, 200],
#     'max_depth': range(1, 6),
#     'learning_rate': [.05, .1, .2, .3, .4, .5, .6 ],
# }

# grid_search = GridSearchCV(estimator= model, param_grid=params, cv=10, n_jobs=-1)

# grid_search.fit(X_train, y_train)          

# print("Best parameters found: ", grid_search.best_params_)
# print("Best accuracy found: ", grid_search.best_score_)



model.fit(X_train, y_train)

print("training set accuracy:",model.score(X_train, y_train))
print("test set accuracy:",model.score(X_test,y_test))

result = model.predict(df_test)
submission = pd.read_csv('data/submission.csv')
submission['Survived']=result
submission.to_csv('data/submissions/submission_gb.csv',index=False)

feature_importance = list(zip(X_train.columns.values, model.feature_importances_))
feature_importance.sort(key=lambda x:x[1])
print(feature_importance)