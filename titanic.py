import pandas as pd
from sklearn.ensemble import GradientBoostingClassifier, AdaBoostClassifier, RandomForestClassifier
from sklearn.tree import DecisionTreeClassifier
from xgboost import XGBClassifier, cv, DMatrix, plot_importance
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis as LDA
from sklearn.model_selection import train_test_split
from sklearn.model_selection import RandomizedSearchCV
from sklearn.preprocessing import StandardScaler
from sklearn import model_selection

df_train = pd.read_csv('data/train-2.csv')
df_test = pd.read_csv('data/test-2.csv')

X = df_train.drop(['Survived'],1)
y = df_train['Survived']

df_train = StandardScaler().fit_transform(df_train)
df_test = StandardScaler().fit_transform(df_test)


X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33, random_state=42)

xgb = XGBClassifier()
gb = GradientBoostingClassifier()
lda = LDA()
rf = RandomForestClassifier()
dtc = DecisionTreeClassifier()
ada = AdaBoostClassifier(DecisionTreeClassifier())

models = [xgb, gb, lda, rf, ada, dtc]
scores = []
for model in models:
    model.fit(X_train, y_train)
    print('training accuracy: ',model.score(X_train, y_train))
    print('test accuracy: ',model.score(X_test,y_test))
    scores.append(model_selection.cross_val_score(model, X_train, y_train, cv=5, scoring='accuracy'))
    
for score in scores:
    print("Kfold : %0.4f (+/- %0.4f)" % (score.mean(), score.std()))



# scores = model_selection.cross_val_score(model, X_train, y_train, cv=5, scoring='accuracy')